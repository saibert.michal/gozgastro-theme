# Starter Theme

#### Description
Description: Starter theme which is grafique-base theme ready.
Theme structure:
* Directory `assets` contains fonts, JS files, CSS/Sass files, images and videos
* Directory `inc` is for parts of code to include
    * custom post types and taxonomies definitions
    * file with scripts and styles enqueueing
    * helper functions file
    * file for inserting marketing specific files
    * shortcodes file with shortcodes definitions
    * file with definitions of custom template tags
    * tgmpa file with specifying plugins which have to be installed
    * theme settings file - settings related to administrator, not for editors
* Directory `lib` for 3rd party libraries
* Directory `template-parts` contains reusable template parts

## Steps
You have to rewrite some parts of this starter theme:
0. First remember you have to install GRAFIQUE Base Theme
1. Then rename starter theme directory
2. `style.css` - Theme name, Description, Tags, GitLab Theme URI
3. `functions.php` - THEME_TD constant (theme context)
4. `screenshot.png` (1024x768) - e.g. site logo on white background
5. `enqueue-scripts-and-styles.php` - rename app handles
6. `sass/_settings.scss` - global settings, breakpoint palette, color palette, ...
7. `sass/_typography.scss` - font face, font variables, define font types, font classes
8. `others` - favicon, kitchen sink, ...

### Best practices
* TODO