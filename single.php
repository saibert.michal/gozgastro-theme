<?php get_header(); ?>
<?php
while ( have_posts() ) : the_post();

$postType = get_post_type( get_the_ID() );

switch($postType){
	case "reference-detail":
	the_reference_detail(get_the_ID());
	break;
	case "aktualita":
	the_new_detail(get_the_ID());
	break;
	case "product":
	$ids 	 = get_queried_object_id();
	$args    = array();
	$terms   = get_terms( 'product_cat', $args );
	//echo '<pre>';print_r($terms);
	?>
	<div class="darkBg">
        <div class="block headPage">
            <div class="row">
                <div class="title">
                    <h1><?php the_title();?></h1>
                    <ol>
                        <li><a href="#">Úvod</a></li>
                        <li><a href="#">Velkokuchyně</a></li>
                        <li>Konvektomaty</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
	<div class="darkBg">
        <div class="catalog darkBg">
			<div class="row">
				<div class="left">
					<?php the_shop_nav_detail_product();?>
	            </div>
				<div class="right"><?php the_content();?></div>
			</div>
		</div>
	</div>
	<?php
	break;
	default:

	break;
}
endwhile;
?>

<?php get_footer(); ?>
