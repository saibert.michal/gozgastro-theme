<?php
/*	Constants
======================================================================== */

# Contexts for WPML
define( 'THEME_TD', "starter-theme-context" );

# Custom post types & taxonomies
define( 'SERVIS_CPT', 'servis' );
define( 'NEWS_CPT', 'aktualita' );
define( 'NEWS_TAX', 'aktualita_c' );
define( 'REFERENCE_CPT', 'reference-detail' );
define( 'REFERENCE_TAX', 'reference_c' );
define( 'PARTNERS_CPT', 'partners_p' );
define( 'TEAM_CPT', 'team_p' );

/*	Includes
======================================================================== */



# Include scripts & styles, helper functions, custom post types and taxonomies, foundation specific functions, clean navigation markup
require_once( 'inc/helper-functions.php' );

require_once( 'inc/custom-post-types-and-taxonomies.php' );

# Global template tags
require_once( 'inc/template-tags.php' );

# Global shortcodes
require_once( 'inc/shortcodes.php' );

# Theme settings page
require_once( 'inc/theme-settings.php' );

# Grafique wanted plugins
require_once( 'inc/tgmpa.php' );

# Grafique image sizes
require_once( 'inc/image-sizes.php' );

# Woocommerce custom functions
require_once('woocommerce/functions.php');

// Include 3rd party librarires
// 'lib/'
/*
register_post_type( 'aktualita_p',
   array(
       'menu_icon' 		=> 'dashicons-admin-page',
       'public' 			=> true,
       'has_archive' 		=> true,
       'capability_type'	=> 'post',
       'hierarchical' 		=> false,
       'supports' 			=> array( 'title', 'editor', 'excerpt', 'thumbnail', 'comments', 'revisions' ),
       'labels' 			=> array(
           'name' 				=> __( 'Aktuality', 'množné číslo' ),
           'singular_name' 	=> __( 'Aktualita', 'jednotné číslo' ),
           'add_new' 			=> __( 'Pridat aktualitu' ),
           'add_new_item' 		=> __( 'Pridat aktualitu' ),
           'edit' 				=> __( 'Upravit aktualitu' ),
           'edit_item' 		=> __( 'Upravit aktualitu' ),
           'new_item' 			=> __( 'Upravit aktualitu' ),
           'view' 				=> __( 'Zobrazit aktualitu' ),
           'view_item' 		=> __( 'Zobrazit aktualitu' ),
           'search_items' 		=> __( 'Hledat aktualitu' ),
           'not_found' 		=> __( 'Nenalezeno' ),
           'not_found_in_trash'=> __( 'Nenalezeno' ),
       ),
   )
);
register_taxonomy( NEWS_TAX, array( 'aktualita_p' ),
	array(
		'labels'            => array(
			'name'              => _x( 'Kategorie', 'množné číslo' ),
			'singular_name'     => _x( 'Kategorie', 'jednotné číslo' ),
			'search_items'      => __( 'Hledat' ),
			'all_items'         => __( 'Vše' ),
			'edit_item'         => __( 'Upravit' ),
			'update_item'       => __( 'Upravit' ),
			'add_new_item'      => __( 'Vytvorit novou kategorii' ),
			'new_item_name'     => __( 'Název' ),
			'menu_name'         => __( 'Kategorie' )
		),
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'public'			=> true,
		'update_count_callback' => function() {
			return; //important
		}
	)
);
*/

require_once( 'inc/enqueue-scripts-and-styles.php' );

function register_acf_options_pages() {

    // Check function exists.
    if( !function_exists('acf_add_options_page') )
        return;

    // register options page.
    $option_page = acf_add_options_page(array(
        'page_title'    => 'Obecné nastavení',
        'menu_title'    => 'Obecné nastavení',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));
}
// Hook into acf initialization.
add_action('acf/init', 'register_acf_options_pages');

function mytheme_admin_bar_render() {
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu('comments');
    $wp_admin_bar->remove_menu('customize');
}
add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );
/*
add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1);
add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1);
add_filter('page_css_class', 'my_css_attributes_filter', 100, 1);
function my_css_attributes_filter($var) {
    return is_array($var) ? array_intersect($var, array('current-menu-item')) : '';
}
*/

/**
 * Manage WooCommerce styles and scripts.
 */
function grd_woocommerce_script_cleaner() {

	// Remove the generator tag
	remove_action( 'wp_head', array( $GLOBALS['woocommerce'], 'generator' ) );
	// Unless we're in the store, remove all the cruft!
	//if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {
		wp_dequeue_style( 'woocommerce_frontend_styles' );
		wp_dequeue_style( 'woocommerce-general');
        wp_dequeue_style( 'woocommerce-general-css');
		wp_dequeue_style( 'woocommerce-layout' );
		wp_dequeue_style( 'woocommerce-smallscreen' );
		wp_dequeue_style( 'woocommerce_fancybox_styles' );
		wp_dequeue_style( 'woocommerce_chosen_styles' );
		wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
		wp_dequeue_script( 'selectWoo' );
        wp_dequeue_script( 'buttons-css' );

		wp_deregister_script( 'selectWoo' );
		wp_dequeue_script( 'wc-add-payment-method' );
		wp_dequeue_script( 'wc-lost-password' );
		wp_dequeue_script( 'wc_price_slider' );
		wp_dequeue_script( 'wc-single-product' );
		wp_dequeue_script( 'wc-add-to-cart' );
		wp_dequeue_script( 'wc-cart-fragments' );
		wp_dequeue_script( 'wc-credit-card-form' );
		wp_dequeue_script( 'wc-checkout' );
		wp_dequeue_script( 'wc-add-to-cart-variation' );
		wp_dequeue_script( 'wc-single-product' );
		wp_dequeue_script( 'wc-cart' );
		wp_dequeue_script( 'wc-chosen' );
		wp_dequeue_script( 'woocommerce' );
		wp_dequeue_script( 'prettyPhoto' );
		wp_dequeue_script( 'prettyPhoto-init' );
		wp_dequeue_script( 'jquery-blockui' );
		wp_dequeue_script( 'jquery-placeholder' );
		wp_dequeue_script( 'jquery-payment' );
		wp_dequeue_script( 'fancybox' );
		wp_dequeue_script( 'jqueryui' );
	//}
}
add_action( 'wp_enqueue_scripts', 'grd_woocommerce_script_cleaner', 99 );

function wps_deregister_styles() {
    wp_dequeue_style( 'buttons-css' );
}
add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );
// Remove WP Version From Styles
add_filter( 'style_loader_src', 'sdt_remove_ver_css_js', 9999 );
// Remove WP Version From Scripts
add_filter( 'script_loader_src', 'sdt_remove_ver_css_js', 9999 );

// Function to remove version numbers
function sdt_remove_ver_css_js( $src ) {
	if ( strpos( $src, 'v=' ) )
		$src = remove_query_arg( 'v', $src );
	return $src;
}

function remove_admin_bar_inline_css() {
   remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', 'remove_admin_bar_inline_css');

add_filter( 'gform_confirmation_anchor', '__return_true' );



function remove_unwanted_css(){
    wp_deregister_style('thickbox-css');
}
add_action('wp_head', 'remove_unwanted_css');
?>
