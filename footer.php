<div id="footer">
    <div class="container">
        <div class="row">
            <div class="col">
                <h3>Informace</h3>
                <ul>
                    <li><a href="">Aktuality</a></li>
                    <li><a href="">Sluzby</a></li>
                    <li><a href="">Servis</a></li>
                    <li><a href="">Reference</a></li>
                    <li><a href="">O nás</a></li>
                    <li><a href="">Partneři </a></li>
                    <li><a href="">Obchodní podmínky </a></li>
                    <li><a href="">Kontakt </a></li>
                </ul>
            </div>
            <div class="col">
                <h3>Produkty</h3>
                <ul>
                    <li><a href="">Velkokuchyně</a></li>
                    <li><a href="">Cukrárny, zmrlinárny</a></li>
                    <li><a href="">Nerezová výroba</a></li>
                    <li><a href="">Bazar produktů</a></li>
                    <li><a href="">Konvektomaty </a></li>
                    <li><a href="">Udržovací skříně </a></li>
                    <li><a href="">Hold-o-mat </a></li>
                    <li><a href="">Sporáky Fritézy</a></li>
                </ul>
            </div>
            <div class="col">
                <ul>
                    <li><a href="">Velkokuchyně</a></li>
                    <li><a href="">Cukrárny, zmrlinárny</a></li>
                    <li><a href="">Nerezová výroba</a></li>
                    <li><a href="">Bazar produktů</a></li>
                    <li><a href="">Konvektomaty </a></li>
                    <li><a href="">Udržovací skříně </a></li>
                    <li><a href="">Hold-o-mat </a></li>
                    <li><a href="">Sporáky Fritézy</a></li>
                </ul>
            </div>
            <div class="col">
                <h3>Kontakty</h3>
                <strong>GOZ GASTRO s.r.o.</strong><br />
                <span>Olomoucká 888/164, 627 00 Brno </span><br />
                <a href="#">+420 545 215 495</a><br />
                <a href="#">info@gozgastro.cz  </a>
            </div>
        </div>
    </div>
</div>
<div id="copyright">
    <div class="container">
        <div class="row">
            <div class="block">
                <p>GOZ GASTRO © 2019. Všechna práva vyhrazena. Vytvořeno ve studiu <a href="">Grafique</a></p>
            </div>
        </div>
    </div>
</div>

<?php
the_shop_categories_nav();
the_mobile_nav();
wp_footer();
?>
<script>
$(document).ready(function() {
    var hash = $(location).attr('hash');
    if(hash.length > 1 && hash == '#gf_1'){
            $( ".tabs ul li a[data-id='order']" ).trigger( "click" );
    }
});
</script>
</body>
</html>
