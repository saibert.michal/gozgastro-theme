<?php
/**
 * Use this template if searching have to be disabled on this site.
 * If so, RENAME this file to search.php!
 *
 * - If this site is "one-page", redirect to site_url()
 * - If this site is not "one-page", redirect to 404
 */

// One-page site? --> Just redirect to HP
// wp_redirect( site_url(), 301 );	// 301 is permanent redirection
// exit();

// No One-page site
global $wp_query;
$wp_query->set_404();
status_header( 404 ); ?>

<?php get_header(); ?>

<?php get_template_part( 'template-parts/404', 'section' ); ?>
		
<?php get_footer(); ?>