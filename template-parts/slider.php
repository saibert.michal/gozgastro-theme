<?php
/**
 * Template Slider
 */

?>
<div id="slider">
	<div class="owl-carousel owl-theme owl-slider">
		<?php foreach($slider as $s){
			$s = $s['title'];
			?>
		<div class="item">
			<div class="text">
				<h2><?=$s['title']?></h2>
				<p><?=$s['description']?></p>
				<br />
				<?php if(!empty($s['url'])){?>
				<a href="<?=$s['url']?>" class="btn"><?=$s['btn']?> <i class="icon icon-arrow-right"></i></a>
				<?php } ?>
			</div>
			<picture alt="slider">
				<source media="(min-width: 981px)" class="owl-lazy" srcset="<?=$s['img-1'];?>" data-srcset="<?=$s['img-1'];?>">
				<source media="(min-width: 501px)" srcset="<?=$s['img-2'];?>">
				<img src="<?=$s['img-3'];?>" alt="<?=$s['title']?>" title="<?=$s['title']?>" />
			</picture>
		</div>
		<? } ?>
	</div>
	<div class="sliderNav">
		<div class="container-fluid">
			<div class="row">
				<?php
				$x = 0;
				foreach($slider as $s){
				$s = $s['title'];
				?>
				<a <?=($x==0) ? 'class="active"' : '';?> data-slide="<?=$x++?>">
					<span><i class="icon <?=$s['nav-ico'];?>"></i><?=$s['nav-title'];?></span>
				</a>
				<?php }?>

			</div>
		</div>
	</div>
</div>
