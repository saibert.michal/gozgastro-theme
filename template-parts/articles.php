<?php
/**
 * Template List of articles in page
 */

 //echo '<pre>'; print_r($articles);
?>
<?php if(!empty($articles)){ ?>
<div class="block articles rev">
<?php

foreach($articles as $article){
	set_query_var( 'block', $article );
	switch($article['acf_fc_layout']){
		case 'img_column':
			get_template_part('/template-parts/articles/img-block');
		break;
		case 'two_column':
			get_template_part('/template-parts/articles/two-block');
		break;
		case 'one_column':
			get_template_part('/template-parts/articles/one-block');

		break;
	}

}
?>
</div>
<?php } ?>
