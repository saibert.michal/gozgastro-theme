<?php
/**
 * 404 section markup
 */
?>

<div class="404-section">
	<div class="grid-x">	

		<!-- Main 404 content -->
		<div class="404-cont small-12 large-8 cell">			
			<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<header>
					<h1 class="page-title"><?php _e( 'Page not found', THEME_TD ); ?></h1>
				</header>
				<div class="main-content">
					<div class="error">
						<p class="bottom"><?php _e( "Page doesn't exist or is currenty unavailable.", THEME_TD ); ?></p>
					</div>
					<p><?php _e( 'You can try following:', THEME_TD ); ?></p>
					<ul> 
						<li><?php _e( 'Check you spelling', THEME_TD ); ?></li>
						<li><?php printf( __( 'Go to <a href="%s">home page</a>', THEME_TD ), home_url() ); ?></li>
						<li><?php _e( 'Return <a href="javascript:history.back()">back</a>', THEME_TD ); ?></li>
					</ul>
				</div>
			</article>
		</div><!-- /.404-cont -->

	</div>
</div><!-- /.404-section -->