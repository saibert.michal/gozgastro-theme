<?php
/**
 * Template Teams
 */
?>
<hr/>
<div class="block contactTeam">
	<div class="row">
		<div class="titles"><h2>Osoby</h2></div>
	</div>
	<div class="row">
		<?php
		foreach($teams as $t){
			$tel  = get_field('telefon',$t->ID);
			$pos  = get_field('pozice',$t->ID);
			$mail = get_field('email',$t->ID);
		?>
			<div class="col">
				<div>
					<img src="<?PHP echo get_the_post_thumbnail_url($t->ID,'thumb');?>" alt="<?=$t->post_title;?>" title="<?=$t->post_title;?>" />
					<h4><?=$t->post_title;?></h4>
					<small><?=$pos;?></small>
				</div>
				<div>
					<?php if(!empty($tel)){?>
					<span>Tel:</span> <a href="tel:<?=$tel?>"><?=$tel?></a><br/>
					<?php } ?>
					<?php if(!empty($mail)){?>
					<span>Email:</span> <a href="mailto:<?=$mail?>"><?=$mail?></a><br/>
					<?php } ?>
				</div>
			</div>
		<?php } ?>
	</div>
</div>
