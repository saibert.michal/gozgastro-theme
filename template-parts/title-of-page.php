<?php
/**
 * Template Title of subpages
 */
?>
<?php if($darkBg){
?>
<div class="darkBg">
<?
}
?>
	<div class="block headPage">
		<div class="row">
			<div class="title">
				<h1><?=$title;?></h1>
				<ol>
					<li><a href="#">Úvod</a></li>
					<li>Aktuality</li>
				</ol>
			</div>
		</div>
		<? if(!empty($subNavs)){?>
		<div class="row">
			<div class="categories">
				<a href="#" class="active">Zobrazit vše</a>
				<a href="#">Aktualně</a>
				<a href="#">Produkty</a>
				<a href="#">Pozvánky</a>
				<a href="#">Legislativa</a>
			</div>
		</div>
		<? } ?>
	</div>
<?php if($darkBg){
?>
</div>	
<?
}
?>
