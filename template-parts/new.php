<?php
/**
 * Template Detail of new
 */
?>
<div class="block">
	<div class="row">
		<div class="article-detail">
			<?=$data->post_content;?>
		</div>
	</div>
	<div class="row">
		<div class="content btns">
			<a href="<?=url('/aktuality/');?>" class="btn"><i class="icon icon-arrow-left"></i> Zpět na přehled novinek </a>
		</div>
	</div>
</div>
