<?php
/**
 * Template News
 */
?>
<?
foreach($partners as $par){
?>
	<div class="client">
		<span>
			<img src="<?PHP echo get_the_post_thumbnail_url($par->ID,'thumb');?>" alt="<?=$par->post_title;?>" title="<?=$par->post_title;?>" />
		</span>
		<h4><a href="<?=get_field('url',$par->ID);?>"><?=$par->post_title;?></a></h4>
	</div>
<? } ?>
