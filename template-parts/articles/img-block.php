<?php
/**
 * Template article 2 columns - text width image
 */
?>
<div class="row">
    <div class="text">
        <?=$block['obsah'];?>
    </div>
    <div class="img">
        <?php
        $img = wp_get_attachment_image_src($block['img'],'thumb')
        ?>
        <?php if(!empty($block['img_link'])){?>
            <img src="<?=$img[0];?>" alt="" title="" />
        <?php }else{?>
            <img src="<?=$img[0];?>" alt="" title="" />
        <? }?>
    </div>
</div>
