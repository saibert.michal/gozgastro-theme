<?php
/**
 * Template product
 */
?>
<?php
if(!empty($products)){
	foreach($products as $p){
	$product   = wc_get_product($p->ID);
	?>
	<div class="product <?=($item) ? 'item' : '';?>">
		<!-- <span class="mark top">Top produkt</span> -->
		<?php
		$img = wp_get_attachment_image_src( get_post_thumbnail_id($product->get_id()));
		?>
		<img src="<?=$img[0];?>" alt="<?=$product->get_name();?>" title="<?=$product->get_name();?>" />
		<h3><a href="<?=get_permalink( $product->get_id() );?>"><?=$product->get_name();?></a></h3>
		<p><?=str_replace(array('<div>','</div>'),'',$product->get_short_description());?></p>
	</div>
<?php
	}
}
?>
