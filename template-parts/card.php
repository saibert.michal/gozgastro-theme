<?php
/**
 * Template - card.
 */
$person = get_query_var("person_data");
?>

<div class="cell small-12 medium-4 large-2 card">
	<div class="person__name text--uppercase">
		<?= $person['name']; ?>
	</div>
</div>