<?php
/**
 * Template Render categories in category page
 */
?>
<div class="row">
<?
foreach($terms as $term){
	$img = wp_get_attachment_image_src(get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true ),'thumbnail',false);
	//$image = wp_get_attachment_url(  );
?>
	<a href="<?= get_term_link($term->term_id); ?>" class="category">
		<span>
			<img src="<?=$img[0];?>" alt="<?=$term->name;?>" title="<?=$term->name;?>" />
			<!-- <img src="img/cat-1.jpg" alt="" title="" /> -->
			<?=$term->name;?>
		</span>
	</a>
<? } ?>
</div>
