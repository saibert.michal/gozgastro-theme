<?php
/**
 * Template headline
 */
?>
<div id="headLine">
	<div class="container">
		<div class="row">
			<div class="content">
				<div class="info">
					<a href="tel:<?php the_field('infolinka', 'option'); ?>"><i class="icon icon-call"></i> <?php the_field('infolinka', 'option'); ?></a>
					<span>|</span>
					<a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a>
					<span>|</span>
					<a href=""><?php the_field('adresa_brno', 'option'); ?></a>
					<span>|</span>
					<a href=""><?php the_field('adresa_praha', 'option'); ?></a>
				</div>
				<div class="social">
					<a href="<?php the_field('facebook', 'option'); ?>" target="_blank" aria-label="Facebook" rel="noreferrer"><i class="icon icon-linkedin"></i></a>
					<a href="<?php the_field('linkedin', 'option'); ?>" target="_blank" aria-label="Linkedin" rel="noreferrer"><i class="icon icon-facebook"></i></a>
				</div>
			</div>
		</div>
	</div>
</div>
