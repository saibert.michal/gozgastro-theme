<?php
/**
 * Template Detail reference
 */

?>
<div class="block">
	<div class="row reference">
		<div class="text">
			<p>
				<?=$data->post_content;?>
			</p>
			<br/>
			<?php if(!empty($cats)){?>
				<?php foreach($cats as $c){?>
					<span class="tag"><?=$c->name;?></span>
				<? } ?>
			<? } ?>
		</div>
		<div class="img">
			<span>
				<img src="<?PHP echo get_the_post_thumbnail_url($data->ID,'thumb');?>" alt="<?=$data->post_title;?>" title="<?=$data->post_title;?>" />
			</span>
		</div>
	</div>
</div>
<?php
$photogalery = get_field('gallery',$data->ID);
if(!empty($photogalery)){?>
<div class="block">
	<div class="row">
		<div class="title center">
			<h2>Fotogalerie z realizace</h2>
		</div>
	</div>
</div>
<div class="block photogalery">
	<div class="row">
		<?php foreach($photogalery as $photo){	?>
			<a href="<?=$photo['url']?>">
				<span>
					<img src="<?=$photo['sizes']['thumbnail']; ?>" alt="<?=$photo['alt']; ?>" />
				</span>
			</a>
		<?php }?>
	</div>
</div>
<?php } ?>
