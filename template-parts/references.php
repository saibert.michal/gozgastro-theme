<?php
/**
 * Template News
 */
?>
<?
foreach($references as $ref){
?>
	<div class="client<?=(isset($class) && !empty($class)) ? ' '.$class : '';?>">
		<span>
			<img src="<?PHP echo get_the_post_thumbnail_url($ref->ID,'thumb');?>" alt="<?=$ref->post_title;?>" title="<?=$ref->post_title;?>" />
		</span>
		<h4><a href="<?php the_permalink($ref->ID); ?>"><?=$ref->post_title;?></a></h4>
	</div>
<? } ?>
