<?php
/**
 * Template Tab attributes of product
 */
?>
<?php
//if(!empty($downloads)){
?>
<div class="tab<?php if($bool) echo ' active';?>" id="tab-order">
    <div class="titles">
        <div><h3>Objednávka produktu</h3></div>
    </div>
    <div class="row">
        <div class="fifty">
            <div class="contacts">
                Email: <a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a><br/>
                Telefon: <a href="tel:<?php the_field('infolinka', 'option'); ?>"><?php the_field('infolinka', 'option'); ?></a>
                <hr/>
                <?php the_field('product_order_description', 'option'); ?>
            </div>
        </div>
        <div class="fifty">
            <h4>Objednávkový formulář</h4>
            <?PHP gravity_form( 1, false, false, $display_inactive = false, $field_values = null, $ajax = false, $tabindex, $echo = true ); ?>
        </div>
    </div>
</div>
<?php //} ?>
