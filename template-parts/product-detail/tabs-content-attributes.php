<?php
/**
 * Template Tab attributes of product
 */
?>
<?php
if(!empty($attributes)){
?>
<div class="tab<?php if($bool) echo ' active';?>" id="tab-params">
    <div class="titles">
        <div><h3>Technické parametry</h3></div>
    </div>
    <div class="row">
        <div class="full">
            <?php foreach($attributes as $a){?>
            <ul>
                <li><?=$a['name']?></li>
                <li><?=$a['options'][0]?></li>
            </ul>
            <?php } ?>
        </div>
    </div>
</div>
<?php } ?>
