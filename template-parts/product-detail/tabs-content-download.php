<?php
/**
 * Template Tab attributes of product
 */
?>
<?php
if(!empty($downloads)){
?>
<div class="tab" id="tab-download">
    <div class="titles">
        <div><h3>Ke stažení</h3></div>
    </div>
    <div class="row">
        <div class="full">
            <?php foreach($downloads as $a){?>
            <ul>
                <li><a href="<?=$a['file']['file']?>"><i class="icon icon-arrow-right"></i><?=$a['file']['name']?></a></li>
            </ul>
            <?php } ?>
        </div>
    </div>
</div>
<?php } ?>
