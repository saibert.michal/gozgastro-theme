<?php
/**
 * Template Tab description
 */
?>
<div class="tab<?php if($bool) echo ' active';?>" id="tab-description">
    <div class="titles">
        <div><h3>Popis produktu</h3></div>
    </div>
    <div class="row">
        <div class="fifty">
            <?=$text;?>
        </div>
        <div class="fifty">
            <?php the_field('product_why', 'option'); ?>
        </div>
    </div>
</div>
