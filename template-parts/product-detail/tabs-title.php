<?php
/**
 * Template Tabs title of product
 */
?>
<?php

if(!empty($titles)){
	?>
	<ul class="tabTitles">
	<?php
	$i = 0;
	foreach($titles as $t){

		$class = 'class="'.$t['title'].'"';
		if($i == 0) $class = 'class="'.$t['title'].' active"';

		if(stristr($t['title'],'descriptions')) $t['title'] = 'description';
		?>
		<li <?=$class?>>
			<a data-id="<?=$t['title']?>"><?=$t['text']?></a>
		</li>
		<?
		$i++;
	}
	?>
	</ul>
	<hr/>
	<?php
}
?>
