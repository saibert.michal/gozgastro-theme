<?php
/**
 * Template Related product
 */
 if(!empty($products)){
?>
<div class="row">
	<div class="title">
		<h2>Podobné produkty</h2>
	</div>
</div>
<div class="row">
	<div class="content">
		<div class="owl-carousel owl-theme owl-related-products">
			<?php foreach($products as $product){?>
			<div class="item product">
				<span class="mark top">Top produkt</span>
				<?php
				$img = wp_get_attachment_image_src( get_post_thumbnail_id($product->get_id()));
				?>
				<img src="<?=$img[0];?>" alt="<?=$product->get_name();?>" title="<?=$product->get_name();?>" />
				<h3><a href="<?=get_permalink( $product->get_id() );?>"><?=$product->get_name();?></a></h3>
				<p><?=str_replace(array('<div>','</div>'),'',$product->get_short_description());?></p>
			</div>
			<?php } ?>
		</div>
	</div>
</div>
<?
}
?>
