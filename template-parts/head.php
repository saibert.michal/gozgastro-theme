<?php
/**
 * Template Header
 */
?>
<div id="header">
	<div class="headContent">
		<div class="row">
			<div class="content">
				<a class="mobileNav"><i class="icon icon-menu"></i></a>
				<a class="search"><i class="icon icon-search"></i></a>
				<a href="<?=url();?>"><img src="<?=img('logo.svg');?>" alt="<?php bloginfo('name');?>" title="<?php bloginfo('name');?>" /> </a>
				<ul class="nav">
					<?php
					wp_nav_menu( array(
						'theme_location'        => 'primary',
						'container'             => false,
						'depth'                 => 0,
						'items_wrap'            => '%3$s',
						'fallback_cb'           => 'gq_base_menu_fallback',
						'walker'                => new gq_base_walker(
							array(
								'in_top_bar'            => true,
								'item_type'             => 'li',
								'menu_type'             => 'main-menu'
							)
						),
					)); ?>
				</ul>
			</div>
		</div>
	</div>
</div>
