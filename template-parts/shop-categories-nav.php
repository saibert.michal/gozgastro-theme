<?php
/**
 * Template Render categories navigation
 */


?>
<div id="productNav">
    <div class="navs">
        <a class="close"><i class="icon icon-close"></i></a>
    </div>
	<?php
	the_shop_categories_nav_render($cats);
	?>
	<!--
    <ul>
        <li>
            <a href="kategorie.html">Velkokuchyně</a>
            <ul>
                <li><a href="kategorie-detail.html">Konvektomaty</a></li>
                <li><a href="kategorie-detail.html">Udržovací skříně Hold-o-mat</a></li>
                <li><a href="kategorie-detail.html">Sporáky</a></li>
                <li><a href="kategorie-detail.html">Fritézy</a></li>
                <li><a href="kategorie-detail.html">Multifunkční pánve FRIMA</a></li>
                <li><a href="kategorie-detail.html">Smažící plotny</a></li>
                <li><a href="kategorie-detail.html">Vodní lázně</a></li>
            </ul>
        </li>
        <li>
            <a href="kategorie.html">Cukrárny, zmrlinárny</a>
            <ul>
                <li><a href="kategorie-detail.html">Výrobníky točené zmrzliny</a></li>
                <li><a href="kategorie-detail.html">Výrobníky kopečkové zmrzliny</a></li>
                <li><a href="kategorie-detail.html">Kombinované výrobníky zmrzliny a pastéry</a></li>
                <li><a href="kategorie-detail.html">Pastéry</a></li>
                <li><a href="kategorie-detail.html">Univerzální vařiče krémů</a></li>
                <li><a href="kategorie-detail.html">Šokery na zmrzlinu</a></li>
                <li><a href="kategorie-detail.html">Zmrzlinové vitríny</a></li>
                <li><a href="kategorie-detail.html">Zákuskové vitríny</a></li>
                <li><a href="/cs/produkty/cukrarny/cukrarske-pece/">Cukrářské pece</a></li>
                <li><a href="/cs/produkty/cukrarny/vyrobniky-ledove-triste/">Výrobníky ledové tříště</a></li>
                <li><a href="/cs/produkty/cukrarny/lednice-cukra/">Chladící zařízení</a></li>
                <li><a href="/cs/produkty/cukrarny/mrazici-zarizeni/">Mrazící zařízení</a></li>
                <li><a href="/cs/produkty/cukrarny/temperovaci-stroje-na-cokoladu/">Temperovací stroje na čokoládu</a></li>
                <li><a href="/cs/produkty/cukrarny/vyrobniky-chlazenych-napoju/">Výrobníky chlazených nápojů</a></li>
            </ul>
        </li>
        <li><a href="kategorie.html">Nerezová výroba</a></li>
        <li><a href="kategorie.html">Bazar</a></li>
    </ul>
	-->
</div>
