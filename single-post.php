<?php get_header(); ?>

<div class="single-news">
	<div href="<?php the_permalink(); ?>" class="reference-row news-row grid-x">
		<div class="portrait-left small-12 medium-2 large-2 columns" style="background-image: url('<?= the_post_thumbnail_url('large'); ?>')">
		</div>
		<div class="content small-12 medium-8 large-8 columns">
			<span class="date"><?= get_the_date(); ?></span>
			<h2><?php the_title(); ?></h2>
			<p class="entry-content"><?php the_content(); ?></p>
		</div>

		<div class="portrait-right small-12 medium-2 large-2 columns"></div>
	</div>
</div>

<?php get_footer(); ?>
