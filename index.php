<?php get_header(); ?>

<div class="default-section">
	<div class="grid-x medium">
		<header class="page__header small-12 cell">
			<h1 class="page__title"><?php the_title(); ?></h1>
		</header>
	</div>	

	<?php while ( have_posts() ) : the_post(); ?>			
		
		<div class="grid-x medium">

			<div class="grid-x small-12 cell"></div>

		</div>

	<?php endwhile; ?> 

</div>
		
<?php get_footer(); ?>
