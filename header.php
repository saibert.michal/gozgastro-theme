<?php

header("Link: </wp-content/themes/gozgastro-theme/assets/styles/all-app-grid.css>; rel=preload; as=style", false);
header("Link: </wp-content/themes/gozgastro-theme/assets/styles/all-app-libs.css>; rel=preload; as=style", false);
header("Link: </wp-content/themes/gozgastro-theme/assets/styles/all-app-theme.css>; rel=preload; as=style", false);
header("Link: </wp-content/themes/gozgastro-theme/assets/styles/app-fonts.css>; rel=preload; as=style", false);

/*
header("Link: </wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen.css>; rel=preload; as=style", false);
header("Link: </wp-content/plugins/woocommerce/assets/css/woocommerce.css>; rel=preload; as=style", false);
header("Link: </wp-content/plugins/woocommerce/assets/css/woocommerce-layout.css>; rel=preload; as=style", false);
*/
header("Link: </wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/style.css>; rel=preload; as=style", false);
header("Link: </wp-includes/css/dist/block-library/style.min.css>; rel=preload; as=style", false);


?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" <?php language_attributes(); ?> > <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" <?php language_attributes(); ?> > <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" <?php language_attributes(); ?> "> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?> > <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo('charset'); ?>">

    <title><?php bloginfo('name'); wp_title('|', true, 'right'); ?></title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="format-detection" content="telephone=no">

    <!--link rel="shortcut icon" type="image/png" href="<?= ''//get_stylesheet_directory_uri(); ?>/favicon.ico"-->
    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/">

    <?php // Include marketing codes
    include( 'inc/marketing.php' ); ?>
    <?php wp_head(); ?>
</head>
<?php
$class = '';
if( current_user_can( 'administrator' ) ){ $class= 'class="isAdmin"';}
?>
<body <?php //body_class('antialiased'); ?> <?=$class?>>
	<?php
		get_template_part('/template-parts/headline','part');
		get_template_part('/template-parts/head','part');
		the_search();
	?>
