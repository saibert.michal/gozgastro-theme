var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var cleanCSS = require('gulp-clean-css');
var browserSync = require('browser-sync').create();

// ------------------------------------------------------

gulp.task('styles', [], function () {
  return gulp.src('styles/main.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({ browsers: ['> 5%'] }))
    .pipe(concat('main.min.css'))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('../styles/'))
    .pipe(browserSync.stream());
});
// ------------------------------------------------------
gulp.task('stylesothers', [], function () {
  return gulp.src(['styles/*.scss','!styles/main.scss'])
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({ browsers: ['> 5%'] }))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('../styles/'))
    .pipe(browserSync.stream());
});
// ------------------------------------------------------
gulp.task('scripts', [], function () {
 return gulp.src(['scripts/jquery.js','scripts/libs/*.js','scripts/scripts.js'])
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(concat('main.min.js'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('../scripts/'));
});

// ------------------------------------------------------

gulp.task('watch', ['styles', 'scripts'], function () {
  gulp.watch('styles/**.scss', ['styles']);
  gulp.watch('styles/compoments/**.scss', ['styles']);
  gulp.watch('scripts/**.js', ['scripts']);
});

// ------------------------------------------------------

// source: https://www.browsersync.io/docs/gulp#gulp-sass-css
gulp.task('browsersync', ['styles','scripts'], function () {
  browserSync.init({ server: '../' });
  gulp.watch('styles/**.scss', ['styles','stylesothers']);
  gulp.watch('styles/compoments/**.scss', ['stylesothers']);
  gulp.watch('scripts/**.js', ['scripts']);
  gulp.watch('../*.html').on('change', browserSync.reload);
});

// ------------------------------------------------------

gulp.task('copyimages', [], function () {
    return   gulp.src('images/*')
            .pipe(gulp.dest('../images/'));
});

// ------------------------------------------------------

gulp.task('default', ['styles', 'stylesothers', 'scripts']);
