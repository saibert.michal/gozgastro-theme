$(document).ready(function() {
   $('.lazy').show().lazy();

   $('.photo').magnificPopup({
       type: 'image',
       gallery: {
           enabled: true
       }
   });

  var owlSlider = $('.owl-slider');

  owlSlider.owlCarousel({
      loop: true,
      margin: 0,
      nav: false,
      dots: true,
      navText: ['<i class="icon icon-arrow-left"><i>','<i class="icon icon-arrow-right"><i>'],
      items: 1,
      autoplay:true,
      animateIn: 'fadeIn',
      animateOut: 'fadeOut',
      autoplayTimeout:5000
  });


  owlSlider.on('changed.owl.carousel', function(event) {
      $('#slider .sliderNav a').removeClass('active');
      $('#slider .sliderNav a:nth-child('+(event.page.index+1)+')').addClass('active');
  });


  $('#slider .sliderNav a').click(function() {
      $('.owl-slider').trigger('to.owl.carousel', $(this).attr('data-slide'));
  });

  var owlProduct = $('.owl-product');
  owlProduct.owlCarousel({
      loop: false,
      margin: 30,
      nav: true,
      dots: true,
      navText: ['<i class="icon icon-arrow-left"><i>','<i class="icon icon-arrow-right"><i>'],
      items: 4,
      autoplay:false,
      stagePadding:15,
      responsive : {
          0 : {
              items: 1
          },
          504 : {
              items: 2
          },
          880 : {
              items: 3
          },
          1100 : {
              items: 4
          }
      }
  });

  var owlProducts = $('.owl-related-products');
  owlProducts.owlCarousel({
      loop: false,
      margin: 30,
      nav: true,
      dots: false,
      navText: ['<i class="icon icon-arrow-left"><i>','<i class="icon icon-arrow-right"><i>'],
      responsive : {
          0 :   { items: 1 },
          376 : { items: 2 },
          640 : { items: 3 }
      },
      autoplay:false
  });

  var owlProducts = $('.owl-products');
  owlProducts.owlCarousel({
      loop: false,
      margin: 30,
      nav: true,
      dots: true,
      navText: ['<i class="icon icon-arrow-left"><i>','<i class="icon icon-arrow-right"><i>'],
      responsive : {
          0 : {
              items: 1
          },
          376 : {
              items: 2
          },
          504 : {
              items: 3
          },
          880 : {
              items: 4
          },
          1100 : {
              items: 5
          }
      },
      autoplay:true,
      autoplayTimeout:5000
  });

  var owlClients = $('.owl-client');
  owlClients.owlCarousel({
      loop: false,
      margin: 10,
      nav: true,
      dots: true,
      navText: ['<i class="icon icon-arrow-left"><i>','<i class="icon icon-arrow-right"><i>'],
      items: 5,
      autoplay:false,
      responsive : {
          320 : {
              items: 1
          },
          330 : {
              items: 2
          },
          504 : {
              items: 3
          },
          880 : {
              items: 4
          },
          1100 : {
              items: 5
          }
      }
  });

  var owlPhotos = $('.owl-photogalery');
  owlPhotos.owlCarousel({
      loop: false,
      margin: 10,
      nav: true,
      dots: true,
      navText: ['<i class="icon icon-arrow-left"><i>','<i class="icon icon-arrow-right"><i>'],
      items: 4,
      autoplay:false
  });

  $(".detailProduct .photos a.order").click(function(){
       $( ".tabs ul li a[data-id='order']" ).trigger( "click" );
  });

  $('input').iCheck({
    checkboxClass: 'icheckbox_square-blue',
    radioClass: 'iradio_square-blue',
    increaseArea: '20%' // optional
  });

  $(".product,.client,.new").click(function(){
       window.location=$(this).find("a").attr("href");
       return false;
  });
  $("#header .search").click(function(){
      obj = $('#search');
      if(obj.hasClass('active')){
          $(this).removeClass('active');
          $('#search').removeClass('active');
      }else{
          $(this).addClass('active');
          $('#search').addClass('active');
      }
  });
  $("#search .close").click(function(){
      $(this).removeClass('active');
      $('#search').removeClass('active');
  });

  $("#header .mobileNav").click(function(){
      $('#mobileNav').addClass('active');
      $('body').addClass('locked');
  });

  $('#mobileNav .close').click(function(){
      $('#mobileNav').removeClass('active');
      $('body').removeClass('locked');
  });

  $(".tabTitles a").click(function(){
      $(".tabTitles li").removeClass('active');
      $(this).closest('li').addClass('active');

      $('.tab').removeClass('active');
      $('.tab#tab-'+$(this).attr('data-id')).addClass('active');
  });



  $('#header a[href="#kategorie"], #mobileNav a[href="#kategorie"]').click( function() {
      $('#productNav').addClass('active');
      return false;
  });

  $('#productNav .navs a.close').click( function() {
      $('#productNav').removeClass('active');
  });


  // Hide Header on on scroll down
  var didScroll;
  var lastScrollTop = 0;
  var delta = 200;
  var navbarHeight = $('#header').outerHeight();

  $(window).scroll(function(event){
        didScroll = true;
   });

   setInterval(function() {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);

    function hasScrolled() {
        var scrollTop = $(this).scrollTop();

        if(scrollTop < lastScrollTop){
            if(scrollTop > delta && !$('#header .headContent').hasClass('fixed')){
                $('#header .headContent').addClass('fixed');
            }else if(scrollTop < delta){
                $('#header .headContent').removeClass('fixed')
                                         .removeClass('scrolled');
            }
        }else{
            if(scrollTop > delta && !$('#header .headContent').hasClass('scrolled')){
                $('#header .headContent').addClass('scrolled');
            }
            if(scrollTop > delta && $('#header .headContent').hasClass('fixed')){
                $('#header .headContent').removeClass('fixed');
            }
        }
        /*
        if(st > delta && !$('#header .headContent').hasClass('scrolled')){
            $('#header .headContent').addClass('scrolled');
        }else{
            $('#header .headContent').removeClass('scrolled');
        }

        if(Math.abs(lastScrollTop - st) <= delta)
            return;

        if (st > lastScrollTop && st > navbarHeight || st < delta){
            $('#header .headContent').removeClass('fixed');

        } else {
            if(st + $(window).height() < $(document).height()) {
                $('#header .headContent').addClass('fixed');
            }
        }
        */
        lastScrollTop = scrollTop;
        console.log(lastScrollTop+ ' - ' + delta);
    }

    // kategorie navigace
    $(".catalog .left a.showCategoriesNav").click(function(){
        $(".catalog .left .nav").addClass('active');
    });
    $(".catalog .left .nav .navs a.close").click(function(){
        $(".catalog .left .nav").removeClass('active');
    });

    if($(window).width() <= 860){
        $(".catalog .left .nav > ul > li > a, #productNav > ul > li > a").click(function(){
            $('.catalog .left .nav > ul > li, #productNav > ul > li').removeClass('active');
            $(this).closest('li').addClass('active');

            if($(this).closest('li').find('ul').length != 0){
                return false;
            }
        });
    }
});
