<?php
/*
 * Template Name: Produkty
 */
?>

<?php

get_header();
the_title_of_page(get_the_title(),true);

if (have_posts()) :
    while (have_posts()): the_post();
?>
<div class="darkBg">
	<div class="catalog">
		<div class="row">
			<div class="left">
				<?php the_shop_nav_detail_product();?>
			</div>
			<div class="right">
				<?php the_products_categories();?>
				<?php the_category_description();?>
                <?php the_content(); ?>
			</div>
		</div>
	</div>
</div>
<?
	endwhile;
endif;
get_footer();
/*
the_references_categories();
the_references();
if (have_posts()) :
	while (have_posts()): the_post();

	endwhile;
endif;
*/

?>
