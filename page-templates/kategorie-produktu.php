<?php
/*
 * Template Name: Kategorie produktu
 */
?>
<?php
get_header();


set_query_var( 'title', get_the_title() );
get_template_part('/template-parts/title-of-page');

if (have_posts()) : ?>
    <?php while (have_posts()): the_post(); ?>
        <div class="block">
            <div class="row">
                <div class="content-detail">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php
endif;


get_footer();
?>
