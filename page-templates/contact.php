<?php
/*
 * Template Name: Kontakt
 */
?>

<?php
get_header();
the_title_of_page(get_the_title());

if (have_posts()) :
	while (have_posts()): the_post();

	endwhile;
endif;
?>
<div class="block contactHead">
	<div class="row">
		<div class="col">
			Email:<a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a>
		</div>
		<div class="col">
			Telefon:<a href="tel:<?php the_field('telefon', 'option'); ?>"><?php the_field('telefon', 'option'); ?></a>
		</div>
		<div class="col">
			Fax:<a href="tel:<?php the_field('fax', 'option'); ?>"><?php the_field('fax', 'option'); ?></a>
		</div>
	</div>
</div>
<?php
$adreses = get_field('adresy');
if(!empty($adreses)){
?>
<hr/>
<div class="block contactAdr">
	<div class="row">
		<?php foreach($adreses as $adr){ ?>
		<div class="col">
			<?=$adr['adresa'];?>
		</div>
		<?php }?>

	</div>
</div>
<?php
}
the_contact_team();
?>

<?php get_footer(); ?>
