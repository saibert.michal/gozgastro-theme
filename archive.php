<?php get_header(); ?>

<div class="archive-section">
	<div class="grid-x">

		<!-- Main archive -->
		<div class="archive-posts small-12 cell">
		
		<?php if ( have_posts() ) : // If some posts available
			while ( have_posts() ) : the_post(); // Posts loop ?>
				
				<article id="post-<?php the_ID(); ?>" <?php post_class('index-card'); ?>>
					<header>
						<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					</header>
					<div class="entry-content">
						<figure><a href="<?php the_permalink(); ?>"><?php has_post_thumbnail() ? the_post_thumbnail('large') : ''; ?></a></figure> <?php the_excerpt(); ?>
					</div>
				</article>
			
			<?php endwhile; // End of posts loop
		else : // If no posts ?>

			<article id="post-0" class="post no-results not-found">
				<header>
					<h2><?php _e( 'Nothing Found', THEME_TD ); ?></h2>
				</header>
				<div class="entry-content">
					<p><?php _e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', THEME_TD ); ?></p>
				</div>
				<hr />
			</article>
			
		<?php endif; // End of posts if ?>
		
		<?php // Display pagination if needed
		if ( is_paged() ) : ?>
			<nav id="post-nav">
				<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', THEME_TD ) ); ?></div>
				<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', THEME_TD ) ); ?></div>
			</nav>
		<?php endif; ?>

		</div><!-- /.archive-posts -->

	</div>
</div><!-- /.archive-section -->
		
<?php get_footer(); ?>