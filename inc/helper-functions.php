<?php
/**
 * Start all functions of this theme
 */

// Only if page template settings enabled
if ( get_option('site_info_module_option') ) {

    /**
     * Creates "Website settings" acf option page in admin.
     *
     * Calls acf function for registering new option page.
     */
    acf_add_options_page(array(
        'page_title' 	=> 'Nastavení údajů',
        'menu_title'	=> 'Nastavení údajů',
        'menu_slug' 	=> 'website-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false,
        'position' 		=> '3.1',
    ));
}

/**
 * Adds role class to body
 * - body class example: role-administrator
 *
 * @hook admin_body_class, body_class
 *
 * @param String	$classes	Clesses of the body
 * @return String				Updated body classes
 */
function add_role_to_body($classes) {
	
    global $current_user;
    
    // Check first, if some roles given for current user
    if ( property_exists($current_user, 'roles') && is_array($current_user->roles) ) {
        
        $user_role = array_shift($current_user->roles); // Returns first role for current user
        
        // Add new class depending on $classes type
        if ( is_array($classes) ) {

            $classes[] = 'role-'. $user_role;

        } elseif ( is_string($classes) ) {

            $classes .= 'role-'. $user_role;
        }
    }

    return $classes;
}
add_filter( 'body_class','add_role_to_body' );
add_filter( 'admin_body_class', 'add_role_to_body' );

// Others functions here ----------------------------------
?>