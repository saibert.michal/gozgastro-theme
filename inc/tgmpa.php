<?php
/**
 * Načtení závislostí
 *
 * @package
 */

require_once get_template_directory() . '/lib/tgmpa/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'mcm_register_required_plugins' );
if ( !function_exists("mcm_register_required_plugins") ) {
    function mcm_register_required_plugins() {
        $plugins = array(
            
            /*array(
                'name'      => 'SVG Support',
                'slug'      => 'svg-support',
                'required'  => true,
                'force_activation'  => true,
            ),
            array(
                'name'      => 'Menu Image',
                'slug'      => 'menu-image',
                'required'  => true,
                'force_activation'  => true,
            ),
            array(
                'name'      => 'FancyBox for Wordpress',
                'slug'      => 'fancybox-for-wordpress',
                'required'  => true,
                'force_activation'  => true,
            ),
            array(
                'name'      => 'Wrap Form Fields In Gravity Forms',
                'slug'      => 'wrap-form-fields-in-gravity-forms',
                'required'  => true,
                'force_activation'  => true,
            ),
            array(
                'name'      => 'Email Obfuscate Shortcode',
                'slug'      => 'email-obfuscate-shortcode',
                'required'  => true,
                'force_activation'  => true,
            ),
            array(
                'name'      => 'Intuitive Custom Post Order',
                'slug'      => 'intuitive-custom-post-order',
                'required'  => true,
                'force_activation'  => true,
            ),*/

        );

        $config = array(
            'id'           => 'mcm',                 // Unique ID for hashing notices for multiple instances of TGMPA.
            'default_path' => '',                      // Default absolute path to bundled plugins.
            'menu'         => 'tgmpa-install-plugins', // Menu slug.
            'parent_slug'  => 'themes.php',            // Parent menu slug.
            'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
            'has_notices'  => true,                    // Show admin notices or not.
            'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
            'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
            'is_automatic' => false,                   // Automatically activate plugins after installation or not.
            'message'      => '',                      // Message to output right before the plugins table.

            /*
            'strings'      => array(
                'page_title'                      => __( 'Install Required Plugins', 'mcm' ),
                'menu_title'                      => __( 'Install Plugins', 'mcm' ),
                /* translators: %s: plugin name. * /
                'installing'                      => __( 'Installing Plugin: %s', 'mcm' ),
                /* translators: %s: plugin name. * /
                'updating'                        => __( 'Updating Plugin: %s', 'mcm' ),
                'oops'                            => __( 'Something went wrong with the plugin API.', 'mcm' ),
                'notice_can_install_required'     => _n_noop(
                    /* translators: 1: plugin name(s). * /
                    'This theme requires the following plugin: %1$s.',
                    'This theme requires the following plugins: %1$s.',
                    'mcm'
                ),
                'notice_can_install_recommended'  => _n_noop(
                    /* translators: 1: plugin name(s). * /
                    'This theme recommends the following plugin: %1$s.',
                    'This theme recommends the following plugins: %1$s.',
                    'mcm'
                ),
                'notice_ask_to_update'            => _n_noop(
                    /* translators: 1: plugin name(s). * /
                    'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
                    'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
                    'mcm'
                ),
                'notice_ask_to_update_maybe'      => _n_noop(
                    /* translators: 1: plugin name(s). * /
                    'There is an update available for: %1$s.',
                    'There are updates available for the following plugins: %1$s.',
                    'mcm'
                ),
                'notice_can_activate_required'    => _n_noop(
                    /* translators: 1: plugin name(s). * /
                    'The following required plugin is currently inactive: %1$s.',
                    'The following required plugins are currently inactive: %1$s.',
                    'mcm'
                ),
                'notice_can_activate_recommended' => _n_noop(
                    /* translators: 1: plugin name(s). * /
                    'The following recommended plugin is currently inactive: %1$s.',
                    'The following recommended plugins are currently inactive: %1$s.',
                    'mcm'
                ),
                'install_link'                    => _n_noop(
                    'Begin installing plugin',
                    'Begin installing plugins',
                    'mcm'
                ),
                'update_link' 					  => _n_noop(
                    'Begin updating plugin',
                    'Begin updating plugins',
                    'mcm'
                ),
                'activate_link'                   => _n_noop(
                    'Begin activating plugin',
                    'Begin activating plugins',
                    'mcm'
                ),
                'return'                          => __( 'Return to Required Plugins Installer', 'mcm' ),
                'plugin_activated'                => __( 'Plugin activated successfully.', 'mcm' ),
                'activated_successfully'          => __( 'The following plugin was activated successfully:', 'mcm' ),
                /* translators: 1: plugin name. * /
                'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'mcm' ),
                /* translators: 1: plugin name. * /
                'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'mcm' ),
                /* translators: 1: dashboard link. * /
                'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'mcm' ),
                'dismiss'                         => __( 'Dismiss this notice', 'mcm' ),
                'notice_cannot_install_activate'  => __( 'There are one or more required or recommended plugins to install, update or activate.', 'mcm' ),
                'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'mcm' ),

                'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
            ),
            */
        );

        tgmpa( $plugins, $config );
    }
}
