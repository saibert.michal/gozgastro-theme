<?php
/**
 * Register and enqueue scripts and styles.
 * Enqueue custom css and js here.
 *
 * @hook wp_enqueue_scripts
 *
 * @return void
 */
function starter_enqueue_style() {

    // 1) Register scripts and styles

    // - app.css

    wp_register_style( 'app-fonts', gq_get_last_version_asset_uri('/assets/styles/app-fonts.css'), array('gq-app'), false, 'all' );
    wp_register_style( 'app-libs', gq_get_last_version_asset_uri('/assets/styles/all-app-libs.css'), array('gq-app'), false, 'all' );
    wp_register_style( 'app-core', gq_get_last_version_asset_uri('/assets/styles/all-app-grid.css'), array('gq-app'), false, 'all' );
    wp_register_style( 'app', gq_get_last_version_asset_uri('/assets/styles/all-app-theme.css'), array('gq-app'), false, 'all' );



    // - foundation.js
    //wp_register_script ( 'foundation', get_stylesheet_directory_uri() . "/assets/js/foundation/foundation.min.js" , array('jquery'), true);

    // - app.js
    wp_register_script( 'app-scripts', gq_get_last_version_asset_uri('/assets/scripts/main.min.js'), array('jquery'), false, true );

    // Page specific scripts and styles example

    // if ( is_front_page() ) {
    //     wp_enqueue_script( '_handle_' );
    // }


    // --> TODO:
    // Enqueue minified assets for production environment
    if ( defined('SITE_ENVIRONMENT') && SITE_ENVIRONMENT == 'production' ) {

    // Enqueue raw assets for development environment (or the option not set)
    } else {

    }


    // 2) Enqueue scripts and styles

    wp_enqueue_style( 'app-fonts');
    wp_enqueue_style( 'app-libs' );
    wp_enqueue_style( 'app-core' );
    wp_enqueue_style( 'app' );

    //wp_enqueue_script( 'foundation' );
    wp_enqueue_script( 'app-scripts' );


    wp_dequeue_style('thickbox-css');

    // Localize scripts example

    //wp_localize_script( 'handle', '_object_name_', array(
    //                '_var_name_' => __( '_Var_text_to_localize_', THEME_TD )
    //) );
}
add_action( 'wp_enqueue_scripts', 'starter_enqueue_style' );

/**
 * Register and enqueue admin styles.
 *
 * @hook admin_enqueue_scripts
 *
 * @return void
 */
function load_custom_wp_admin_style() {

    // Register style
    wp_register_style( 'custom-wp-admin-css', get_stylesheet_directory_uri() . '/assets/css/admin.css?v=' . filemtime(get_stylesheet_directory() . '/assets/css/admin.css'), false);

    // Enqueue style
    wp_enqueue_style( 'custom-wp-admin-css' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );

function loadCss($type = 1){

}

?>
