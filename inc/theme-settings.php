<?php
/**
 * Child Theme admin settings page
 */

function add_theme_settings_page() {
	add_submenu_page( 'themes.php', 'Nastavení šablony', 'Nastavení šablony', 'manage_options', 'theme_settings_page', 'theme_settings_page' );
}
add_action( 'admin_menu', 'add_theme_settings_page' );

function theme_settings_page() {

	// Current theme object
	$theme = wp_get_theme(); ?>

	<div class="wrap">
	    <h1>Nastavení <span style="font-style:italic;">child</span> šablony: <?php print_r( $theme->display('Name') ); ?></h1>
	    <form method="post" action="options.php">
	        <?php
                // General
	            settings_fields("general-section");
	            do_settings_sections("theme-options");
	            submit_button(); 
	        ?>          
	    </form>
	</div>

<?php }

function site_info_module() { ?>

	<input type="checkbox" name="site_info_module_option" value="1" <?php checked(1, get_option('site_info_module_option'), true); ?> /><br>
	<p class="theme-settings-description">Vytvoří ACF options page s názvem "Nastavení údajů".</p>

<?php }

function display_theme_settings_fields() {

    // OTHERS section
	add_settings_section("general-section", "Základní", null, "theme-options");
	
	// Web settings admin page
    add_settings_field("site_info_module_option", "Modul nastavení údajů", "site_info_module", "theme-options", "general-section");
	register_setting("general-section", "site_info_module_option");
}
add_action( 'admin_init', 'display_theme_settings_fields' );
?>