<?php
/**
 * Tiny functions, output processors and helpers used all across the theme.
 */

// -------------------EXAMPLE------------------------------

/**
 * Function description
 *
 * @return void
 */
/*function the_persons() {

    // Call template part
    set_query_var("person_data",$person);
    get_template_part("template-parts/person","card");
}*/

// -----------------END OF EXAMPLE-------------------------


/**
 * Render images from folder assets/img
 *
 * @param string      name of file
 * @return string
 */
function img($url){
    return get_stylesheet_directory_uri('').'/assets/img/'.$url;
}




/**
 * Get link of page
 *
 * @param string      name of file
 * @return string
 */
function url($url = false){
    return get_site_url().$url;
}

/**
 * Render adv on page
 *
 *
 * @return string
 */
function the_adv($ids){
    $banner = get_field('banner','option');
    $render = get_field('render_banner',$ids);

    if(!empty($render)){
        echo $banner;
    }
}

/**
 * Render products
 *
 *
 * @return string
 */
function the_products($n){
    echo do_shortcode('[products limit="'.$n.'" columns="12"]');
}

/**
 * Render products
 *
 *
 * @return string
 */
function the_products_categories(){
    global $post;

    $ob = get_queried_object();

    if(isset($ob->post_type) && !empty($ob->post_type)) $ids = 0;
    else                                                $ids = get_queried_object_id();

    $args = array(
         'taxonomy'     => 'product_cat',
         'number'       => '',
         'hierarchical' => true,
         'child_of'     => 0,
         'orderby'      => 'menu_order',
         'order'        => 'ASC',
         'parent'       => $ids
    );
    $terms = get_terms( $args );
    if(!empty($terms)){
        set_query_var( 'terms', $terms );
        get_template_part('/template-parts/shop-categories');
    }
}

/**
 * Render description of category
 *
 *
 * @return string
 */
function the_category_description(){
    global $post;
    $description = get_field('category_description',$post->ID);
    if(!empty($description)){
    ?>
    <div class="row">
        <div class="description">
            <div>
                <?=$description;?>
            </div>
        </div>
    </div>
    <?php
    }
}





/**
 * Render team in contact page
 *
 * @return string
 */
function the_contact_team(){
    $args = array(
         'post_type'   => 'team_p',
         'posts_per_page' => 12,
         'post_status' => 'publish',
         'orderby'		=>  'id',
         'order'		=>  'DESC'
    );
    $wp_query = new WP_Query( $args );
    $posts = $wp_query->get_posts();
    if(!empty($posts)){
        set_query_var( 'teams', $posts );
        get_template_part('/template-parts/teams');
    }
}

/**
 * Render adv of catalog
 *
 * @return string
 */
function the_shop_adv(){
    $advs = get_field('banner_products','option');
    if(!empty($advs)){
        foreach($advs as $adv){
            $adv = $adv['banner'];
        ?>
        <div class="advs">
            <div>
                <big><?=$adv['title']?></big>
                <p><?=$adv['text']?></p>
                <a href="<?=$adv['url']?>" class="btn"><?=$adv['btn']?> <i class="icon icon-arrow-right"></i></a>
            </div>
            <img src="<?=$adv['obrazek']?>" data-speed="1" class="img-parallax">

        <?php

        ?>
        </div>
        <?php
        }
    }
}


/**
 * Render serach line
 *
 * @return string
 */
function the_search(){
    ?>
    <div id="search">
        <div class="block">
            <div class="row">
                <form action="#">
                    <div>
                        <a href="#" class="close"><i class="icon icon-close"></i></a>
                        <input type="text" name="q" value="" autocomplete="off" placeholder="Zadejte text pro vyhledávaní" itemprop="query-input">
                        <button>Hledat</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?
}

/**
 * Render render detail of reference
 * @param id of reference
 * @return string
 */
function the_reference_detail($ids){
    $data = get_post($ids);

    set_query_var( 'title', $data->post_title );
    get_template_part('/template-parts/title-of-page');
    set_query_var('cats',get_the_terms($ids,'reference_c'));
    set_query_var( 'data', $data );
    get_template_part('/template-parts/reference');
    //print_r($posts);
    //echo $posts->ID;
}
/**
 * Render render categories of references
 *
 * @return string
 */
function the_references_categories(){
    $terms = get_terms(
        array(
            'taxonomy'   => 'reference_c',
            'hide_empty' => false
        )
    );
    if(!empty($terms)){
        set_query_var( 'terms', $terms );
        get_template_part('/template-parts/nav-categories','part');
    }
}
/**
 * Render render news
 *
 * @return string
 */
 function the_references(){
     $args = array(
     	  'post_type'   => 'reference-detail',
     	  'posts_per_page' => 12,
     	  'post_status' => 'publish',
     	  'orderby'		=>  'id',
     	  'order'		=>  'DESC'
 	 );
     $wp_query = new WP_Query( $args );
     $posts = $wp_query->get_posts();
     ?>
     <div class="block">
     	<div class="row">
             <?php
             if(!empty($posts)){
                 set_query_var( 'references', $posts );
                 get_template_part('/template-parts/references','part');
             }
             ?>
        </div>
        <div class="row">
            <div class="content btns">
                <a href="<?=url('/reference/');?>" class="btn">Zobrazit další reference <i class="icon icon-arrow-right"></i></a>
            </div>
        </div>
     </div>
     <?php
 }
 get_template_part('/template-parts/articles/one-block');
 /**
  * Render render artlices on Home page
  *
  * @return string
  */
 function the_articles_on_home(){
     set_query_var( 'articles', get_field('obsah') );
     get_template_part('/template-parts/articles');
 }
/**
 * Render render references on Home page
 *
 * @return string
 */
function the_reference_on_home(){
    $args = array(
         'post_type'   => 'reference-detail',
         'numberposts' => 10,
         'post_status' => 'publish',
         'orderby'		=>  'id',
         'order'		=>  'DESC'
    );
    $wp_query = new WP_Query( $args );
    $posts = $wp_query->get_posts();
    ?>
    <div class="block">
        <div class="row">
            <div class="title">
                <h2>Naši spokojení klienti</h2>
            </div>
        </div>
        <div class="row">
            <div class="content">
                <div class="owl-carousel owl-theme owl-client">
                    <?php
                    if(!empty($posts)){
                        set_query_var( 'references', $posts );
                        set_query_var( 'class', 'item' );
                        get_template_part('/template-parts/references','part');
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="content btns">
                <a href="<?=url('/reference/');?>" class="btn">Zobrazit reference <i class="icon icon-arrow-right"></i></a>
            </div>
        </div>
    </div>
    <?
}
/**
 * Render render products on Home page
 *
 * @return string
 */
function the_porducts_on_home(){
    $args = array(
         'post_type'   => 'product',
         'posts_per_page' => 12,
         'post_status' => 'publish',
         'orderby'		=>  'id',
         'order'		=>  'DESC'
    );
    $wp_query = new WP_Query( $args );
    $posts = $wp_query->get_posts();
    if(!empty($posts)){
    ?>
    <div class="block">
        <div class="row">
            <div class="title">
                <h2>Akční nabídka produktů</h2>
            </div>
        </div>
        <div class="row">
            <div class="owl-carousel owl-theme owl-product">
                <?
                set_query_var( 'item', true );
                set_query_var( 'products', $posts );
                get_template_part('/template-parts/product');
                ?>
            </div>
        </div>
        <div class="row">
            <div class="content btns">
                <a href="<?=url('/obchod/');?>" class="btn">Zobrazit nabídku <i class="icon icon-arrow-right"></i></a>
            </div>
        </div>
    </div>
    <?
    }
}
/**
 * Render render title of page
 * @paramas title
 * @return string
 */
function the_title_of_page($title,$dark  = false){
    set_query_var( 'darkBg', $dark );
    set_query_var( 'title', $title );
    get_template_part('/template-parts/title-of-page');
}
/**
 * Render render categories of news
 *
 * @return string
 */
function the_news_categories(){
    $terms = get_terms(
        array(
            'taxonomy'   => 'aktualita_c',
            'hide_empty' => false
        )
    );
    if(!empty($terms)){
        set_query_var( 'terms', $terms );
        get_template_part('/template-parts/nav-categories','part');
    }
}
/**
 * Render render detail of reference
 * @param id of reference
 * @return string
 */
function the_new_detail($ids){
    $data = get_post($ids);

    set_query_var( 'title', $data->post_title );
    get_template_part('/template-parts/title-of-page');
    set_query_var( 'data', $data );
    get_template_part('/template-parts/new','part');
}
/**
 * Render render news
 *
 * @return string
 */
 function the_news(){
     $args = array(
     	  'post_type'   => 'aktualita',
     	  'posts_per_page' => 12,
     	  'post_status' => 'publish',
     	  'orderby'		=>  'id',
     	  'order'		=>  'DESC'
 	 );
     $wp_query = new WP_Query( $args );
     $posts = $wp_query->get_posts();
     ?>
     <div class="block">
     	<div class="row">
             <?php
             if(!empty($posts)){
                 set_query_var( 'news', $posts );
                 get_template_part('/template-parts/news','part');
             }
             ?>
        </div>
        <div class="row">
            <div class="content btns">
                <a href="<?=url('/aktuality/');?>" class="btn">Zobrazit další novinky <i class="icon icon-arrow-right"></i></a>
            </div>
        </div>
     </div>
     <?php
 }

/**
 * Render render products on Home page
 *
 * @return string
 */
function the_news_on_home(){

    $args = array(
         'post_type'   => 'aktualita',
         'numberposts' => 4,
         'post_status' => 'publish',
         'orderby'		=>  'id',
         'order'		=>  'DESC'
    );
    $wp_query = new WP_Query( $args );
    $posts = $wp_query->get_posts();
    ?>
    <div class="block">
        <div class="row">
            <div class="title">
                <h2>Aktuality</h2>
                <a href="<?=url('/aktuality/');?>" class="btn">Zobrazit další aktuality <i class="icon icon-arrow-right"></i></a>
            </div>
        </div>
        <div class="row">
            <?php
            if(!empty($posts)){
                set_query_var( 'news', $posts );
                get_template_part('/template-parts/news','part');
            }
            ?>
        </div>
    </div>
    <?
}
/**
 * Render render partners
 *
 * @return string
 */
 function the_partners(){
     $args = array(
     	  'post_type'   => 'partners_p',
     	  'posts_per_page' => -1,
     	  'post_status' => 'publish',
     	  'orderby'		=>  'id',
     	  'order'		=>  'DESC'
 	 );
     $wp_query = new WP_Query( $args );
     $posts = $wp_query->get_posts();
     ?>
     <div class="block">
     	<div class="row">
             <?php
             if(!empty($posts)){
                 set_query_var( 'partners', $posts );
                 get_template_part('/template-parts/partners','part');
             }
             ?>
        </div>
     </div>
     <?php
 }
/**
 * Render slider
 *
 * @return string
 */
function the_slider(){
    $slider = get_field('slider','option');
    if(!empty($slider)){
        set_query_var( 'slider', $slider );
        get_template_part('/template-parts/slider');
    }
}
/**
 * Get tree of opens categories
 * @param  object of tax, retrun arg
 * @return array
 */

function the_shop_nav_tree($parent){
    static $argRet = [];

    if ( $parent != 0 ) {
        $t = get_term( $parent, 'product_cat' );
        $argRet[] = $t->term_id;
        the_shop_nav_tree($t->parent);
    }

    return $argRet;
}

/**
 * Render categories of products
 *
 * @return string
 */
function the_shop_nav_detail_product(){
    global $post;
    global $product;

    $arg = array(
         'taxonomy'     => 'product_cat',
         'number'       => '',
         'hierarchical' => true,
         'child_of'     => 0,
         'orderby'      => 'menu_order',
         'order'        => 'ASC',
         'parent'       => 0
    );

    $cats  = get_terms($arg);

    $inCat = array();
    $inCat = wp_get_post_terms( $post->ID, 'product_cat', array('fields' => 'ids') );

    if(empty($inCat)){
        $inCat = the_shop_nav_tree(get_queried_object(),array());
    }

    if(!empty($cats)){
        ?>
        <a class="showCategoriesNav"><i class="icon icon-menu"></i> Zobrazit kategorie produktů</a>
        <div class="nav">
            <div class="navs">
                <a class="close"><i class="icon icon-close"></i></a>
            </div>
            <?php the_shop_nav_detail_product_subcategories($cats,$inCat,$arg); ?>
        </div>
        <?php
    }
}

function the_shop_nav_detail_product_subcategories($cats,$arg,$opt){
    ?>
    <ul>
        <?php
        foreach($cats as $a){
            $class = '';
            if(isset($arg) && in_array($a->term_id,$arg)) $class = 'class="active"';
            ?>
            <li <?=$class;?>>
                <a href="<?=get_category_link($a->term_id);?>"><?=$a->name?></a>
                <?php
                if(isset($arg) && in_array($a->term_id,$arg)){
                    $opt['parent'] = $a->term_id;
                    $catsR = get_terms($opt);
                    if(!empty($catsR)){
                        the_shop_nav_detail_product_subcategories($catsR,$arg,$opt);
                    }
                }
                ?>
            </li>
            <?
        }
        ?>
    </ul>
    <?
}

/**
 * Helper function for the_shop_categories_nav
 * @param id of childrens
 * @return string
 */
function the_shop_categories_nav_recursive($ids){
    $argRet = array();

    if ( $ids != 0 ) {
        $par = array(
            'taxonomy'     => 'product_cat',
            'number'       => '',
            'hierarchical' => true,
            'child_of'     => $ids,
            'orderby'      => 'menu_order',
            'order'        => 'ASC',
            'parent'       => $ids);

        $ts = get_terms( $par );

        if(!empty($ts)){
            foreach($ts as $t){
                $argRet[] = array('category'=>$t, 'sub'=> the_shop_categories_nav_recursive($t->term_id));
            }
        }
        unset($ts);
    }


    return $argRet;
}

/**
 * Get categories of products
 *
 * @return string
 */
function the_shop_categories_nav(){
    $argRet = array();

    $arg = array(
        'taxonomy'     => 'product_cat',
        'number'       => '',
        'hierarchical' => true,
        'child_of'     => 0,
        'orderby'      => 'menu_order',
        'order'        => 'ASC',
        'parent'       => 0
    );


    $cats  = get_terms($arg);
    foreach($cats as $c){
        $sub = the_shop_categories_nav_recursive($c->term_id);
        $args['category'] = $c;
        if(!empty($sub)) $args['sub'] = $sub;
        $argRet[] = $args;
        unset($args);
    }

    set_query_var( 'cats', $argRet );
    get_template_part('/template-parts/shop-categories-nav');
}
/**
 * Render nav categories of products
 * @param  categories
 * @return string
 */
 function the_shop_categories_nav_render($cats){

    // echo '<pre>'.print_r($cats, true)."</pre>";

     if(!empty($cats)){
         ?>
         <ul>
         <?php
         foreach($cats as $c){
            ?>
            <li>
                <a href="<?=get_term_link($c['category']);?>"><?=$c['category']->name;?></a>
            <?php
            if(!empty($c['sub'])){
                the_shop_categories_nav_render($c['sub']);
            }
            ?>
            </li>
            <?
         }
         ?>
         </ul>
         <?php
     }
 }

 function the_mobile_nav(){
     ?>
     <div id="mobileNav">
         <div class="navs">
             <a class="close"><i class="icon icon-close"></i></a>
         </div>
         <ul>
             <?php
             wp_nav_menu( array(
                 'theme_location'        => 'primary',
                 'container'             => false,
                 'depth'                 => 0,
                 'items_wrap'            => '%3$s',
                 'fallback_cb'           => 'gq_base_menu_fallback',
                 'walker'                => new gq_base_walker(
                     array(
                         'in_top_bar'            => true,
                         'item_type'             => 'li',
                         'menu_type'             => 'main-menu'
                     )
                 ),
             ));
             ?>
         </ul>
     </div>
     <?php
 }
