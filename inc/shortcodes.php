<?php
/**
 * Defined shortcodes
 *
 * @source https://codex.wordpress.org/Shortcode_API
 */

// -------------------EXAMPLE------------------------------

/**
 * Shortcode example
 *
 * @use: [shortcode_name foo="foo-value"]
 *
 * @param $atts     array   Parameters
 * @return custom
 */
// function shortcode_function( $atts ) {
// 	$a = shortcode_atts( array(
// 		'foo' => 'something',       // Default value
// 		'bar' => 'something else',
// 	), $atts );
// 	return "foo = {$a['foo']}";
// }
// add_shortcode( 'shortcode_name', 'shortcode_function' );

// -----------------END OF EXAMPLE-------------------------

function company_values_shortcode($atts) {
    $atr = shortcode_atts(
                array(
                   'numbers' => '',
                   'texts' => '',
                 ),
                $atts
            );
    //print_r($atr);
    $nrs = explode(',',$atr['numbers']);
    $tes = explode(',',$atr['texts']);

    $str = '<div class="values">';
    $i = 0;
    foreach($nrs as $n){
        $str .= '<div class="value">
                    <big>'.$n.'</big>
                    '.$tes[$i].'
                </div>';
        $i++;
    }

    $str .= '</div>';
    return $str;
}
add_shortcode( 'company-values', 'company_values_shortcode' );

function service_column_shortcode($atts, $content = null) {
    $atr = shortcode_atts(array('img' => ''), $atts);
    return '<div><span><img src="'.img($atr['img']).'" alt="" title="">'.str_replace('<br>','',$content).'</span></div>';
}
add_shortcode( 'service-column', 'service_column_shortcode' );

function service_columns_shortcode($atts, $content = null) {
    return '<div class="service-columns">'.do_shortcode( $content ).'</div>';
}
add_shortcode( 'service-columns', 'service_columns_shortcode' );


//[internal-menu-target id='1,2,3,4' text='text 1, text 2, text 3, text 4']
