<?php
/**
 * Register custom post types and taxonomies
 *
 * @source https://codex.wordpress.org/Function_Reference/register_post_type
 * @source https://codex.wordpress.org/Function_Reference/register_taxonomy
 */

// -------------------EXAMPLE------------------------------

// [CPT] Products
/*register_post_type( '_POST_TYPE_SLUG_',
	array(
		'menu_icon' 		=> 'dashicons-admin-tools',
		'public' 			=> true,
		// 'has_archive' 		=> true,
		'capability_type'	=> 'post',
		'hierarchical' 		=> false,
		//'rewrite' 			=> array( 'slug' => 'produkty' ),
		'supports' 			=> array( 'title' ),
		'labels' 			=> array(
			'name' 				=> __( 'Produkty' ),
			'singular_name' 	=> __( 'Produkt' ),
			'add_new' 			=> __( 'Přidat produkt' ),
			'add_new_item' 		=> __( 'Přidat' ),
			'edit' 				=> __( 'Upravit' ),
			'edit_item' 		=> __( 'Upravit' ),
			'new_item' 			=> __( 'Upravit' ),
			'view' 				=> __( 'Zobrazit' ),
			'view_item' 		=> __( 'Zobrazit' ),
			'search_items' 		=> __( 'Hledat' ),
			'not_found' 		=> __( 'Nenalezeno' ),
			'not_found_in_trash'=> __( 'Nenalezeno' ),
		),
	)
);*/

// (TAX) Reference sector
/*register_taxonomy( '_TAXONOMY_SLUG_', array( '_POST_TYPE_SLUG_' ),
	array(
		'labels'            => array(
			'name'              => _x( 'Sektory', 'množné číslo' ),
			'singular_name'     => _x( 'Sektor', 'jednotné číslo' ),
			'search_items'      => __( 'Hledat' ),
			'all_items'         => __( 'Všechny' ),
			'edit_item'         => __( 'Upravit' ),
			'update_item'       => __( 'Upravit' ),
			'add_new_item'      => __( 'Vytvořit nový sektor' ),
			'new_item_name'     => __( 'Název' ),
			'menu_name'         => __( 'Sektory' )
		),
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true
	)
);*/

// -----------------END OF EXAMPLE-------------------------
register_post_type( SERVICE_CPT,
	array(
		'menu_icon' 		=> 'dashicons-location-alt',
		'public' 			=> true,
		'has_archive' 		=> true,
		'capability_type'	=> 'post',
		'hierarchical' 		=> false,
		'supports' 			=> array( 'title', /*'editor', 'excerpt', 'thumbnail',*/ /*'comments',*/ 'revisions' ),
		'labels' 			=> array(
			'name' 				=> __( 'Servis', 'množné číslo' ),
			'singular_name' 	=> __( 'Servis', 'jednotné číslo' ),
			'add_new' 			=> __( 'Přidat servis' ),
			'add_new_item' 		=> __( 'Přidat servis' ),
			'edit' 				=> __( 'Upravit servis' ),
			'edit_item' 		=> __( 'Upravit servis' ),
			'new_item' 			=> __( 'Upravit servis' ),
			'view' 				=> __( 'Zobrazit servis' ),
			'view_item' 		=> __( 'Zobrazit servis' ),
			'search_items' 		=> __( 'Hledat servis' ),
			'not_found' 		=> __( 'Nenalezeno' ),
			'not_found_in_trash'=> __( 'Nenalezeno' ),
		),
	)
);
register_post_type( NEWS_CPT,
	array(
		'menu_icon' 		=> 'dashicons-format-aside',
		'public' 			=> true,
		'has_archive' 		=> true,
		'capability_type'	=> 'post',
		'hierarchical' 		=> false,
		'supports' 			=> array( 'title', 'editor', 'excerpt', /*'thumbnail',*/ /*'comments',*/ 'revisions' ),
		'labels' 			=> array(
			'name' 				=> __( 'Aktauality', 'množné číslo' ),
			'singular_name' 	=> __( 'Aktualita', 'jednotné číslo' ),
			'add_new' 			=> __( 'Přidat aktualitu' ),
			'add_new_item' 		=> __( 'Přidat aktualitu' ),
			'edit' 				=> __( 'Upravit aktualitu' ),
			'edit_item' 		=> __( 'Upravit aktualitu' ),
			'new_item' 			=> __( 'Upravit aktualitu' ),
			'view' 				=> __( 'Zobrazit aktualitu' ),
			'view_item' 		=> __( 'Zobrazit aktualitu' ),
			'search_items' 		=> __( 'Hledat aktualitu' ),
			'not_found' 		=> __( 'Nenalezeno' ),
			'not_found_in_trash'=> __( 'Nenalezeno' ),
		),
	)
);
register_taxonomy( NEWS_TAX, array( 'aktualita' ),
	array(
		'labels'            => array(
			'name'              => _x( 'Kategorie', 'množné číslo' ),
			'singular_name'     => _x( 'Kategorie', 'jednotné číslo' ),
			'search_items'      => __( 'Hledat' ),
			'all_items'         => __( 'Vše' ),
			'edit_item'         => __( 'Upravit' ),
			'update_item'       => __( 'Upravit' ),
			'add_new_item'      => __( 'Vytvorit novou kategorii' ),
			'new_item_name'     => __( 'Název' ),
			'menu_name'         => __( 'Kategorie' )
		),
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'public'			=> true,
		'update_count_callback' => function() {
			return; //important
		}
	)
);
register_post_type( REFERENCE_CPT,
	array(
		'menu_icon' 		=> 'dashicons-format-aside',
		'public' 			=> true,
		'has_archive' 		=> false,
		'capability_type'	=> 'post',
		'hierarchical' 		=> false,
		'supports' 			=> array( 'title', 'editor', /*'excerpt',*/ 'thumbnail', /*'comments',*/ 'revisions' ),
		'labels' 			=> array(
			'name' 				=> __( 'Reference', 'množné číslo' ),
			'singular_name' 	=> __( 'Reference', 'jednotné číslo' ),
			'add_new' 			=> __( 'Přidat referenci' ),
			'add_new_item' 		=> __( 'Přidat referenci' ),
			'edit' 				=> __( 'Upravit referenci' ),
			'edit_item' 		=> __( 'Upravit referenci' ),
			'new_item' 			=> __( 'Upravit referenci' ),
			'view' 				=> __( 'Zobrazit referenci' ),
			'view_item' 		=> __( 'Zobrazit referenci' ),
			'search_items' 		=> __( 'Hledat referenci' ),
			'not_found' 		=> __( 'Nenalezeno' ),
			'not_found_in_trash'=> __( 'Nenalezeno' ),
		),
	)
);
register_taxonomy( REFERENCE_TAX, array( 'reference-detail' ),
	array(
		'labels'            => array(
			'name'              => _x( 'Kategorie', 'množné číslo' ),
			'singular_name'     => _x( 'Kategorie', 'jednotné číslo' ),
			'search_items'      => __( 'Hledat' ),
			'all_items'         => __( 'Vše' ),
			'edit_item'         => __( 'Upravit' ),
			'update_item'       => __( 'Upravit' ),
			'add_new_item'      => __( 'Vytvorit novou kategorii' ),
			'new_item_name'     => __( 'Název' ),
			'menu_name'         => __( 'Kategorie' )
		),
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'public'			=> true,
		'update_count_callback' => function() {
			return; //important
		}
	)
);
register_post_type( PARTNERS_CPT,
	array(
		'menu_icon' 		=> 'dashicons-admin-multisite',
		'public' 			=> true,
		'has_archive' 		=> false,
		'capability_type'	=> 'post',
		'hierarchical' 		=> false,
		'supports' 			=> array( 'title', 'editor', /*'excerpt',*/ 'thumbnail', /*'comments',*/ 'revisions' ),
		'labels' 			=> array(
			'name' 				=> __( 'Partneři', 'množné číslo' ),
			'singular_name' 	=> __( 'Partner', 'jednotné číslo' ),
			'add_new' 			=> __( 'Přidat partnera' ),
			'add_new_item' 		=> __( 'Přidat partnera' ),
			'edit' 				=> __( 'Upravit partnera' ),
			'edit_item' 		=> __( 'Upravit partnera' ),
			'new_item' 			=> __( 'Upravit partnera' ),
			'view' 				=> __( 'Zobrazit partnera' ),
			'view_item' 		=> __( 'Zobrazit partnera' ),
			'search_items' 		=> __( 'Hledat partnera' ),
			'not_found' 		=> __( 'Nenalezeno' ),
			'not_found_in_trash'=> __( 'Nenalezeno' ),
		),
	)
);
register_post_type( TEAM_CPT,
	array(
		'menu_icon' 		=> 'dashicons-admin-multisite',
		'public' 			=> true,
		'has_archive' 		=> false,
		'capability_type'	=> 'post',
		'hierarchical' 		=> true,
		'supports' 			=> array( 'title', 'editor', /*'excerpt',*/ 'thumbnail', /*'comments',*/ 'revisions' ),
		'labels' 			=> array(
			'name' 				=> __( 'Team', 'množné číslo' ),
			'singular_name' 	=> __( 'Team', 'jednotné číslo' ),
			'add_new' 			=> __( 'Přidat člověka' ),
			'add_new_item' 		=> __( 'Přidat člověka' ),
			'edit' 				=> __( 'Upravit člověka' ),
			'edit_item' 		=> __( 'Upravit člověka' ),
			'new_item' 			=> __( 'Upravit člověka' ),
			'view' 				=> __( 'Zobrazit člověka' ),
			'view_item' 		=> __( 'Zobrazit člověka' ),
			'search_items' 		=> __( 'Hledat člověka' ),
			'not_found' 		=> __( 'Nenalezeno' ),
			'not_found_in_trash'=> __( 'Nenalezeno' ),
		),
	)
);
