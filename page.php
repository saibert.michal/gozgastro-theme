<?php get_header(); ?>


<?php
$postType = get_post_type( get_the_ID() );

$tax = 'default';
$tax = get_query_var( 'taxonomy' );


if (have_posts()) :
    while (have_posts()): the_post();
        switch($tax){
            case "product_cat":
                the_title_of_page(get_the_title(),true);
                ?>
                <div class="darkBg">
                    <div class="catalog">
                        <div class="row">
                            <div class="left">
                                <?php the_shop_nav_detail_product();?>
                                <?php the_shop_adv();?>
                            </div>
                            <div class="right">
                                <? the_products_categories();?>
                                <? the_category_description();?>
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?
            break;
            default:
                set_query_var( 'title', get_the_title() );
                get_template_part('/template-parts/title-of-page');
                ?>
                <div class="block">
                    <div class="row">
                        <div class="content-detail">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
                <?
            break;
        }
    endwhile;
endif;

set_query_var( 'articles', get_field('obsah') );
get_template_part('/template-parts/articles');
?>



<?php
the_reference_on_home();

get_footer(); ?>
