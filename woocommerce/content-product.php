<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// --> TODO: get this before loop, not in every loop item
$current_user = wp_get_current_user();

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
//echo '<pre>'; print_r($product);
?>
<div class="product">
	<div>
		<span class="mark top">Top produkt</span>
		<?php
		$img = wp_get_attachment_image_src( get_post_thumbnail_id($product->get_id()),'product-thumbs');
		?>
		<img src="<?=$img[0];?>" alt="<?=$product->get_name();?>" title="<?=$product->get_name();?>" />
		<h3><a href="<?=get_permalink( $product->get_id() );?>"><?=$product->get_name();?></a></h3>
		<p><?=str_replace(array('<div>','</div>'),'',$product->get_short_description());?></p>
	</div>
</div>

<!-- <li <?php wc_product_class( '', $product ); ?>> -->
	<?php // Set course data
	/*
	$category_selector = '';
	$categories = array();

	$categories_array = wp_get_post_terms(get_the_ID(), 'product_cat');
	if ( isset($categories_array[0]) ) {
		$category_selector = ' ' . get_category_selector( $categories_array[0], 'product_cat' );
	}
	$categories = is_wp_error($categories_array) ? array() : implode(' ', array_map(function ($category) { return $category->name; }, $categories_array));

	$course = array(
		'thumbnail'		=> gal_product_placeholder($product->get_id()),
		'price'			=> false,
		'title'			=> get_the_title(),
		'permalink'		=> get_permalink(),
		'author'		=> get_the_author_meta( 'display_name', $product->post->post_author),
		'categories'	=> $categories,
		'cat_selector'	=> $category_selector,
		'price'			=> wc_price($product->get_regular_price()),
		'length'		=> '20min'	// --> TODO: get course lenght (from acf or videos?)
	);
	*/
	//set_query_var('course', $course);
	//get_template_part('/template-parts/product');
	/**
	 * Hook: woocommerce_before_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	//do_action( 'woocommerce_before_shop_loop_item' );

	/**
	 * Hook: woocommerce_before_shop_loop_item_title.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	// do_action( 'woocommerce_before_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	// do_action( 'woocommerce_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	// do_action( 'woocommerce_after_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	// do_action( 'woocommerce_after_shop_loop_item' );
	?>

	<!-- <p><?= ''//get_the_author_meta( 'display_name', $product->post->post_author); ?></p> -->
<!-- </li> -->
