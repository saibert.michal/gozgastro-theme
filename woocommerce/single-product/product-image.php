<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $product;

$imgT = wp_get_attachment_image_src( get_post_thumbnail_id($product->get_id()),'product-thumb');
$imgF = wp_get_attachment_image_src( get_post_thumbnail_id($product->get_id()),'oreginal');

$galery = $product->get_gallery_attachment_ids();
?>
<a class="img photo" href="<?=$imgF[0]?>">
	<img src="<?=$imgT[0];?>" alt="" title="" />
</a>
<?php
if(!empty($galery)){
?>
<div class="owl-carousel owl-theme owl-photogalery">
	<?php
	foreach( $galery as $g ) {
	$imgT = wp_get_attachment_image_src( $g,'thumbnail');
	$imgF = wp_get_attachment_image_src( $g,'original');
	?>
	<a class="item photo" href="<?=$imgF[0]?>">
		<img src="<?=$imgT[0]?>" alt="" title="" />
	</a>
	<?php }?>
</div>
<?php
}
?>
<a class="order">
    <i class="icon icon-info"></i>
    <strong>Máte zájem o tento produkt?</strong><br>
    Zanechte nám na sebe kontakt.<br>
    My Vám zašleme nabídku.
</a>
