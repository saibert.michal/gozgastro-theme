<?php

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);


remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 15);


function goz_product_tabs_render_titles(){
    global $post;
    global $product;

    $isService = get_field('show_tab_service',$post->ID);

    $desc = get_field('text',$post->ID);
    $down = get_field('download',$post->ID);
    $atrs = $product->get_attributes();

    if(!empty($desc)) $arg[] = array('title'=>'descriptions','text'=>'Popis produktu');
    if(!empty($atrs)) $arg[] = array('title'=>'params','text'=>'Technické parametry');
    $arg[] = array('title'=>'order','text'=>'Objednávka produktu');
    if(!empty($down)) $arg[] = array('title'=>'download','text'=>'Ke stažení');
    if(!empty($isService[0]) && $isService[0] == 1) $arg[] = array('title'=>'servis','text'=>'Servis');

    set_query_var( 'titles', $arg );
    get_template_part('/template-parts/product-detail/tabs-title');
}
add_action( 'goz_product_tabs', 'goz_product_tabs_render_titles', 1 );

function goz_product_tabs_render_content(){
    global $post;
    global $product;
    $bool = true;
    set_query_var( 'bool', $bool);

    $isService = get_field('show_tab_service',$post->ID);

    $desc = get_field('text',$post->ID);
    $down = get_field('download',$post->ID);
    $atrs = $product->get_attributes();

    if(!empty($desc)){
        set_query_var( 'text', get_field('text',$post->ID));
        get_template_part('/template-parts/product-detail/tabs-content-description');
        $bool = false;
    }
    if(!empty($atrs)){
        set_query_var( 'attributes', $product->get_attributes());
        get_template_part('/template-parts/product-detail/tabs-content-attributes');
        $bool = false;
    }

    set_query_var( 'bool', $bool);
    get_template_part('/template-parts/product-detail/tabs-content-order');

    if(!empty($down)){
        set_query_var( 'downloads', $down);
        get_template_part('/template-parts/product-detail/tabs-content-download');
    }

    if(!empty($isService[0]) && $isService[0] == 1){
        get_template_part('/template-parts/product-detail/tabs-content-services');
    }

}
add_action( 'goz_product_tabs', 'goz_product_tabs_render_content', 2 );

function goz_product_related_render(){
    global $post;
    global $product;

    $argRet = array();
    $arg    = wc_get_related_products($product->id);

    if(!empty($arg)){
        foreach($arg as $a){
            if(!empty($a)) $argRet[] = wc_get_product($a);
        }
    }
    set_query_var( 'products', $argRet);
    get_template_part('/template-parts/product-detail/related-products');
}
add_action( 'goz_product_related', 'goz_product_related_render', 1 );



/*
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 ); // price
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );  // e.g. category
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 ); // excerpt
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 ); // excerpt
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 ); // tabs
add_action( 'woocommerce_after_single_product', 'woocommerce_output_related_products', 17 );

function galoty_course_attributes() {
    global $post;
    global $product;
    ?>
	<div class="row">
        <div class="columns large-6 medium-12 small-12">
            <h2><?= __('Autor',THEME_TD); ?></h2>
            <?php the_product_author($post->post_author); ?>
        </div>
        <div class="columns large-6 medium-12 small-12 rates">
            <?php // Rating
            $course_rating = get_post_meta($post->ID,'galoty_course_rating',true);
            if ( empty($course_rating) ) {  // --> TODO: Remove this
                $course_rating = array('value'=>4.6,'total'=>23);
            }
            $value = round(floatval($course_rating['value']),1);
            // update_post_meta($post->ID, 'galoty_course_rating', array('value'=>0,'total'=>0));
            set_query_var('rating', array(
                'value'         => $value,
                'stars'         => get_rating_stars($value),
                'total'         => '['.intval($course_rating['total']) .' '. __('Hodnocení',THEME_TD) . ']',
                'rating_link'   => '#'  // --> TODO: move to rating section
            ));
            get_template_part( '/template-parts/course-detail/course', 'rating' ); ?>
        </div>
    </div>
<?php }
add_action( 'woocommerce_after_single_product_summary', 'galoty_course_attributes', 5 );
*/
?>
